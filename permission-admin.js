const credentials = require('../common.js').credentials.admin;
const commonTasks = require('../common-tasks.js')

module.exports = {
	before: commonTasks.login(credentials.account, credentials.username, credentials.password),

	"Can create a workspace": function (browser) {

	},

	after: function(browser) {
		browser.end();
	}
};