module.exports = {
	credentials: {
		bogus: {
			account: 'qa',
			username: 'bogus',
			password: 'bogus'
		},
		admin: {
			account: 'qa',
			username: 'QAAdminUser',
			password: 'T35t3r@890'
		},
		accountAdmin: {
			account: 'qa',
			username: 'QAAccountAdmin',
			password: 'T35t3r@890'
		},
		dataAdmin: {
			account: 'qa',
			username: 'QADataAdminUser',
			password: 'T35t3r@890',
			workspaceId: 2,
			coreTableId: 1,
			dataTableId: 31
		},
		OTPTokenTest: {
			account: 'qa',
			username: 'QATokenTest',
			password: 'T35t3r@890'
		}
	},
}