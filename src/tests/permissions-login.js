const credentials = require('../common.js').credentials;

const makeGoodLoginFunc = (account, username, password, withLogout) => (browser) => {
	const login = browser.page.login();
	const loginForm = login.section.loginForm;
	const logout = login.section.successfulLoginElement;

	loginForm
		.login(account, username, password)
		.waitForElementVisible('body', 10000, false, function() {
			login.expect.section('@successfulLoginElement').to.be.present;

			if(withLogout) {
				// then logout
				browser.page.logout()
					.navigate()
					.waitForElementVisible('body', 10000, false);
			}
		});
}

// Dupe credentials and remove bogus account leaving only, and thus creating, goodAccounts
const { bogus, OTPTokenTest, ...goodAccounts } = credentials; 

const tests = {
	before: function (browser, done) {
		browser.page.login()
			.navigate()
			.waitForElementVisible('body', 10000, false, function() {
				done();
			});
	},

	"Can't login as bogus user": function (browser) {
		const login = browser.page.login();
		const loginForm = login.section.loginForm;

		loginForm
			.login(credentials.bogus.account, credentials.bogus.username, credentials.bogus.password)
			.api.pause(2000);
		
		login.expect.section('@successfulLoginElement').to.not.be.present;
	},

	"Can't login as admin with wrong password": function (browser) {
		const login = browser.page.login();
		const loginForm = login.section.loginForm;

		loginForm
			.login(credentials.admin.account, credentials.admin.username, 'bogus')
			.api.pause(2000);
		
		login.expect.section('@successfulLoginElement').to.not.be.present;
	},

	after: function(browser) {
		browser.end();
	}
};

// Add duplicated tests procedurally to the test object
Object.assign(tests, ...Object.keys(goodAccounts).map(accountName=>{
	const account = goodAccounts[accountName];

	const obj = {}
	obj[`Can log in as ${account.username}`] = makeGoodLoginFunc(account.account, account.username, account.password, true);

	return obj;
}))

module.exports = tests;