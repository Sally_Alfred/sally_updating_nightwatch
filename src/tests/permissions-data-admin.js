const credentials = require('../common.js').credentials.dataAdmin;
const commonTasks = require('../common-tasks.js');

module.exports = {
	'@tags': ['permission1'],

	beforeEach: commonTasks.login(credentials.account, credentials.username, credentials.password),

	"Can't access workspace settings": function (browser) {
		const workspace = browser.page.workspace();
		const workspaceUrl = workspace.url(credentials.workspaceId);
		const tabs = workspace.section.tabs;
		workspace.waitForElementVisible('body', 30000, false, function() {
			browser.url(workspaceUrl);
			tabs.expect.element('@settingsLink').to.not.be.present;
		});
	},

	"Can't create a workspace": function (browser) {
		const workspaces = browser.page.workspaces();
		const administrationLinks = workspaces.section.administrationLinks;
			administrationLinks.expect.element('@createWorkspace').to.not.be.present;
	},

	"Can't access admin tab": function (browser) {
		const admin = browser.page.admin();
		admin.navigate();
	
		admin.expect.element('@error').to.be.present;
		admin.expect.element('@error').text.to.equal('An error has occurred');
	},

	"Can download core table data": function (browser) {
		const coretable = browser.page.coreTableData();
		const dataControls = coretable.section.dataControls;

		const newUrl = coretable.url(credentials.workspaceId, credentials.coreTableId);

		browser.url(newUrl);
		dataControls.expect.element('@downloadLink').to.be.visible;
	},

	"Can navigate through to a core table contact's data tab": function (browser) {
		const coretable = browser.page.coreTable();
		const coretableData = browser.page.coreTableData();
		const coretableContact = browser.page.coreTableContact();

		const tabs = coretable.section.tabs;
		const table = coretableData.section.table;
		const contactTabs = coretableContact.section.tabs;

		const newUrl = coretable.url(credentials.workspaceId, credentials.coreTableId);
	
		browser
			.url(newUrl)
			.waitForElementVisible('body', 10000, false)
		tabs.expect.element('@browseTab').to.be.visible;
		
		tabs.navigateToBrowse().waitForElementVisible('body', 10000, false);
		table.navigateToFirstItem().waitForElementVisible('body', 10000, false);

		contactTabs.expect.element('@contactDataTab').to.be.visible;
	},

	"Can edit data table permissions and settings": function (browser) {
		const datatable = browser.page.dataTable();
		const newDataTableUrl = datatable.url(credentials.workspaceId, credentials.dataTableId);
		const tabs = datatable.section.tabs;

		browser
			.url(newDataTableUrl)
			.waitForElementVisible('body', 10000, false)	
		
		tabs.expect.element('@settingsTab').to.be.visible;
	},

	"Can create a data table": function (browser) {
		const datatables = browser.page.dataTables();
		const adminLinks = datatables.section.administrationLinks

		const newDataTablesUrl = datatables.url(credentials.workspaceId);

		browser
			.url(newDataTablesUrl)
			.waitForElementVisible('body', 10000, false);	

			adminLinks.expect.element('@createDataTableLink').to.be.visible;
	},
	
	"Can access core table settings": function (browser) {
		const coretable = browser.page.coreTable();
		const tabs = coretable.section.tabs;

		const newUrl = coretable.url(credentials.workspaceId, credentials.coreTableId);
	
		browser.url(newUrl).waitForElementVisible('body', 10000, false, function() {
			tabs.expect.element('@settingsTab').to.be.visible;
		});
	},

	"Can access link to create core table": function (browser) {
		const coretables = browser.page.coreTables();
		const administrationLinks = coretables.section.administrationLinks;

		const newUrl = coretables.url(credentials.workspaceId);
		
		browser.url(newUrl).waitForElementVisible('body', 10000, false, function() {
			 administrationLinks.expect.element('@createCoretable').to.be.visible;
		});
	},

	afterEach: function(browser) {
		browser.end();
	}
};