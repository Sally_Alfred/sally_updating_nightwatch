const credentials = require('../common.js').credentials.accountAdmin;
const commonTasks = require('../common-tasks.js');

module.exports = {
	'@tags': ['permission'],

	beforeEach: commonTasks.login(credentials.account, credentials.username, credentials.password),
	
	"Can access admin tab": function (browser) {
		const workspaces = browser.page.workspaces();
		const workspaceTabs = workspaces.section.tabs;
		workspaces.navigate().waitForElementVisible('body', 10000, false, function() {
		workspaceTabs.expect.element('@adminTab').to.be.visible;
		
		});		
	},

	"Can access link to create user": function (browser) {
		const admin = browser.page.admin()
				admin.navigate().waitForElementVisible('body', 10000, false, function() {
			admin.expect.element('@createUserLink').to.be.visible;
		});		
	},

	"Can access link to disable user": function (browser) {
		const user = browser.page.user()
		
		user.navigate().waitForElementVisible('body', 10000, false, function() {
			user.expect.element('@disableForm').to.be.visible;
		});		
	},

	"Can access user permissions tab": function (browser) {
		const user = browser.page.user()
		
		user.navigate().waitForElementVisible('body', 10000, false, function() {
		user.expect.element('@permissionsTab').to.be.visible;
		});		
	},

	"Can access link to create workspace": function (browser) {
		const workspaces = browser.page.workspaces();
		const administrationLinks = workspaces.section.administrationLinks;

		workspaces.navigate().waitForElementVisible('body', 10000, false, function() {
			administrationLinks.expect.element('@createWorkspace').to.be.visible;
		});		
	},

	"Can make a workspace inactive": function (browser) {
		const workspace = browser.page.workspaceSettings();
		workspace.navigate();
	
		const form = workspace.section.form;
		const main = workspace.section.main;
	
		form.toggleActiveState().waitForElementVisible('body', 20000, false, function() {
			main.expect.element('@notice').to.be.visible;
			main.expect.element('@notice').text.to.equal('This workspace is currently inactive');
		});

		// toggle it back again
		workspace.navigate().waitForElementVisible('body', 10000, false, function() {
			form.toggleActiveState();
		});
	},

	"Cannot create a core table": function (browser) {
		const coretables = browser.page.coreTables();
		const administrationLinks = coretables.section.administrationLinks;

		coretables.navigate().waitForElementVisible('body', 10000, false, function() {
			administrationLinks.expect.element('@createCoretable').to.not.be.present;
		});
	},

	afterEach: function(browser) {
		browser.end();
	}
};