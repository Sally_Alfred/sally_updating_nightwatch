require('../common.js');

module.exports = {
	'@tags': ['login'],
	before: function (browser, done) {
		browser.page.login()
			.navigate()
			.waitForElementVisible('body', 10000, false, function() {
				done();
			});
	},

	"Login pane not in forgotten password mode": function (browser) {
		const login = browser.page.login();
		login.expect.section('@loginForm').to.be.visible;
	},

	"Login page has username and password fields": function (browser) {
		const login = browser.page.login();
		const loginForm = login.section.loginForm;

		loginForm.expect.element('@usernameField').to.be.visible;
	},

	"Reset prompt appears when requested": function (browser) {
		const login = browser.page.login();
		const loginForm = login.section.loginForm;

		loginForm.resetPassword();
		login.expect.element("h1").to.be.visible;
		login.expect.element("h1").text.to.equal('Reset password request');
		// login.expect.section('@resetForm').to.be.visible;
	},

	after: function(browser) {
		browser.end();
	}
};