module.exports = {
	url: function(workspaceId = 1748) { 
		return `${this.api.launchUrl}/Qa/workspace/${workspaceId}/data/datatable/list`; 
	},
	sections: {
		tabs: {
			selector: '.tabs',
			elements: {
				settingsTab: {
					selector: 'Settings',
					locateStrategy: 'link text'
				}
				
			}
		},
		administrationLinks: {
			selector: '.main__sidebar',
			elements: {
				createDataTableLink: {
					selector: 'Create new data table',
				locateStrategy: 'link text'
			}
			}
		}
	}	
};