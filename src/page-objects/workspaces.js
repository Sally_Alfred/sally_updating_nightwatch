module.exports = {
	url: function() { 
		
		return this.api.launchUrl + '/Qa/workspace'; 
		
		
	},
	sections: {
		tabs: {
			
			selector: '.primary-nav__list',

			elements: {
               adminTab: {
						selector: 'li.primary-nav__item.primary-nav__item--selected',
						locateStrategy: 'css selector'
				}
			}
		},


		administrationLinks: {
			selector: '.sidebar-nav',
			elements: {
				createWorkspace: {
					selector: 'Create new workspace',
					locateStrategy: 'link text'
			
			}

			}
		}
	}	
};