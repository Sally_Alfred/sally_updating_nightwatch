module.exports = {
	url: function(workspaceId = 1748, tableId = 1) { 
		return `${this.api.launchUrl}/Qa/workspace/${workspaceId}/data/coretable/${tableId}/edit`; 
	},
	sections: {
		administrationLinks: {
			selector: '.sidebar-section .nav-list',
			elements: {
				createCoretable: 'a[href *= "data/coretable/create"]'
			}
		}
	}	
};