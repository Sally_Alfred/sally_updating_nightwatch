const commands =  {
	navigateToDataTables: function () {
		this
			.click('@dataTablesLink')
			.api.pause(1000);

		return this;
	}
}

module.exports = {
	
	url: function(workspaceId = '1748') { 
		return `${this.api.launchUrl}/Qa/workspace/${workspaceId}/view`; 
	},

	sections: {
		tabs: {
			 selector: '#bar-section .header-tabs',
			elements: {
				dataTablesLink: 'a[href *= "data/datatable/list"]',
				settingsLink: {
					selector: 'Settings',
					locateStrategy: 'link text'
				}
			},
			commands: [commands]
		}
	}
};