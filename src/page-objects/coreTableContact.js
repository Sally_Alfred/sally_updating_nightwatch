module.exports = {
	url: function(workspaceId = 1748, tableId = 1, contactId = 1) { 
		return `${this.api.launchUrl}/Qa/workspace/${workspaceId}/data/coretable/${tableId}/contact/${contactId}/map`; 
	},
	sections: {
		tabs: {
			selector: '.tabs',
			elements: {
				contactDataTab: {
					selector: 'Contact Data',
					locateStrategy: 'link text'
				}
			},
		}
	}	
};