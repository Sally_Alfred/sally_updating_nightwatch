module.exports = {
	url: function() { 
		return this.api.launchUrl + '/Qa/admin/user/260/view'; 
	},
	elements: {
		disableForm: {
			
			selector:'//span[contains(text(),"Disable user")]',
			locateStrategy: 'xpath'
		},

		permissionsTab: {
			selector:'Permissions',
			locateStrategy: 'link text'
		}
	}
};
