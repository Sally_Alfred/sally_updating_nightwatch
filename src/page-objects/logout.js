module.exports = {
	url: function() { 
		return this.api.launchUrl + '/Qa/logout'; 
	},
	elements: {
		logoutLink: '#logout'
	}
};
