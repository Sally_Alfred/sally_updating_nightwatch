module.exports = {
	url: function(workspaceId = 1748) { 
		return `${this.api.launchUrl}/Qa/workspace/${workspaceId}/data/coretable/list`; 
	},

	sections: {

		administrationLinks: {
			selector: '.main__sidebar',

			elements: {
				createCoretable: {
					selector: 'Create new core table',
				locateStrategy: 'link text'
		}
		}
	}	}
};