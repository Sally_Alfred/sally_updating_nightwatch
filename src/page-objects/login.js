const loginCommands =  {
	resetPassword: function () {
		this
			.click('@forgottenPasswordLink')
			.api.pause(1000);

		return this;
	},
	login: function (account, username, password) {
		this
			.clearValue('@accountField').setValue('@accountField', account)
			.clearValue('@usernameField').setValue('@usernameField', username)
			.clearValue('@passwordField').setValue('@passwordField', password)
			.click('@submit')
		
		return this;
	}
}

const logoutCommands = {
	logout: function () {
		this
			.click('@logoutLink')

		return this;
	}
}

module.exports = {
	url: function() { 
		return this.api.launchUrl + '/'; 
	},
	sections: {
		loginForm: {
			selector: '#login-form',
			elements: {
				errorMessage: '.messages .error',
				forgottenPasswordLink: 'a[href="/auth/reset_password_request"]',
				accountField: '#account',
				usernameField: '#username',
				passwordField: '#password',
				submit: '#login-form button[type="submit"]'
			},
			commands: [loginCommands]
		},
		resetForm: {
			selector: '//h1[text()="Reset password request"]'
		},
		successfulLoginElement: {
			selector: '#logout',
			elements: {
				logoutLink: '#logout'
			},
			commands: [logoutCommands]
		}
	}	
};