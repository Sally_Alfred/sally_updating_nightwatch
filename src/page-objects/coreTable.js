const tabCommands = {
	navigateToBrowse: function (account, username, password) {
		this.click('@browseTab')
		
		return this;
	}
}

module.exports = {
	url: function(workspaceId = 1748, tableId = 1) { 
		return `${this.api.launchUrl}/Qa/workspace/${workspaceId}/data/coretable/${tableId}/view`; 
	},
	sections: {
		tabs: {
			selector: '.tabs',
			elements: {
				settingsTab: {
					selector: 'Settings',
					locateStrategy: 'link text'
				},
				browseTab: {
					selector: 'Browse',
					locateStrategy: 'link text'
				}
			},
			commands: [tabCommands]
		},
		dataControls: {
			selector: '#layout-content .list .controls-bottom',
			elements: {
				downloadLink: {
					selector: 'a.download-link'
				}
			}
		}
		// administrationLinks: {
		// 	selector: '.sidebar-section .nav-list',
		// 	elements: {
		// 		createCoretable: 'a[href *= "data/coretable/create"]'
		// 	}
		// }
	}	
};