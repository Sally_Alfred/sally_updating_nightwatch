const tabCommands = {
	navigateToBrowse: function (account, username, password) {
		this.click('@browseTab')
		
		return this;
	}
}

const tableCommands = {
	navigateToFirstItem: function (account, username, password) {
		this.click('@firstItemLink')
		
		return this;
	}
}

module.exports = {
	url: function(workspaceId = 1748, tableId = 1) { 
		return `${this.api.launchUrl}/Qa/workspace/${workspaceId}/data/coretable/${tableId}/dataviewer`; 
	},
	sections: {
		tabs: {
			selector: '#layout-content .tabset',
			elements: {
				settingsTab: {
					selector: 'Settings',
					locateStrategy: 'link text'
				},
				browseTab: {
					selector: 'Browse',
					locateStrategy: 'link text'
				}
			},
			commands: [tabCommands]
		},
		table: {
			selector: '.table__body',
			elements: {
				firstItemLink: {
					selector: 'tbody tr td:first-child a'
				}
			},
			commands: [tableCommands]
		},
		dataControls: {
			selector: '.table-controls__group',
			elements: {
				downloadLink: {
					selector: 'button[contains(., "Download")]',
					locateStrategy: 'xpath'
				}
			}
		}
		// administrationLinks: {
		// 	selector: '.sidebar-section .nav-list',
		// 	elements: {
		// 		createCoretable: 'a[href *= "data/coretable/create"]'
		// 	}
		// }
	}	
};