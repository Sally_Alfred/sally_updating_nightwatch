module.exports = {
	url: function(workspaceId = 1748, tableId) { 
		return `${this.api.launchUrl}/Qa/workspace/${workspaceId}/data/datatable/${tableId}/view`;
	},
	sections: {
		tabs: {
			selector: '.tabs',
			elements: {
				settingsTab: {
					selector: 'Settings',
					locateStrategy: 'link text'
				}
				
			}
		}
	}	
};