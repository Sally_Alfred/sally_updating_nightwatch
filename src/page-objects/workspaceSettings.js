const commands = {
	toggleActiveState: function () {
		this.click('@activeCheckbox');
		this.click('@submitButton')

		return this;
	},
	save: function() {}
}

module.exports = {
	url: function() { 
		return this.api.launchUrl + '/Qa/workspace/1748/edit'; 
	},
	sections: {
		main: {
			selector: '.alert--warning',
			elements: {
				notice: '.alert__content'
			}
		},
		form: {
			selector: '.form',
			elements: {
				
				activeCheckbox: '#active',
				submitButton: '#field_submit input'
			},
			commands: [commands]
		}
	}	
};