module.exports = {
	login: function(account, username, password) {
		return function (browser, done) {
			var login = browser.page.login();
			var loginForm = login.section.loginForm;

			browser.page.login()
				.navigate()
				.waitForElementVisible('body', 2000, false, function() {
					done()
				})
				
			loginForm
				.login(account, username, password)
				.waitForElementVisible('body', 2000, false, function(){
					done();
				});
		}
	},
}