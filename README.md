# Nightwatch E2E tests

## Installation (Mac/Linux)

1. Install NVM: https://github.com/creationix/nvm#installation
2. Run `npm i`

## Usage

1. Run `nvm use`
2. Run `npm run test-debug` 

## Scripts (`npm run X`)

* `test-debug` runs tests in chrome in a visible window.
* `test` runs tests in chrome headlessly.

To run an individual test file:

* `npm run [test|test-debug] ./src/tests/[filename]`